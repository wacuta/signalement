# Signalement

> Ce projet à été généré avec [Angular CLI](https://github.com/angular/angular-cli) version 15.0.1

## Pré-requis

- [Node.js](https://nodejs.org)
- Lancer la commande `npm i`
- [Angular CLI](https://github.com/angular/angular-cli)

## Run

Lancer le serveur avec la commande `ng serve`. Puis aller sur le lien [http://localhost:4200/](http://localhost:4200/). 

## Build

Pour build la projet, lancer la commande `ng build`. Le resultat sera dans le répertoir `dist/`.

## Lancer les Tests unitaires

Pour lancer les tests unitaire, lancer la commande `ng test`. Les tests sont executés avec [Karma](https://karma-runner.github.io).

## TODO
- [x] mettre en place le routing, app ne doit rien avoir
- [x] Resolver pour l'edition de signalement
- [x] heritage des composant 
- [x] genre/sexe Enum
- [x] nommage des interfaces
- [ ] pour le select des observations, faire un composant perso
- [ ] mat-select utilise compareBy pour pré selectionner
- [ ] Formgrouop dans le formgroup pour Author
- [ ] submit du form, juste un button dans le form
- [ ] Injection provinIn root a prosecrire
- [ ] test injection dependence dans providers
- [ ] "Smart & Dumb component"
- [ ] Voir pourquoi le `PUT` retourne une réponse `null`
- [ ] Comprendre comment fonctionne les TU et en faire plus.
- [ ] mettre en place les tests _end-to-end_
