import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { MatSnackBarModule } from '@angular/material/snack-bar';

import { ValidatorPersoService } from './validator-perso.service';

describe('ValidatorPersoService', () => {
  let service: ValidatorPersoService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        MatSnackBarModule
      ]
    });
    service = TestBed.inject(ValidatorPersoService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
