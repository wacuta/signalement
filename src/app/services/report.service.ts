import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Report } from '../entities/report.interface';
import { catchError, map } from 'rxjs/operators';
import { AlerteSnackBarService } from './alerte-snack-bar.service';


@Injectable({
  providedIn: 'root'
})
export class ReportService {

  private reportsUrl : string = 'api/reports';
  private httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };

  constructor(
    private http: HttpClient,
    private alerte: AlerteSnackBarService
  ) { }


  getReports(): Observable<Report[]> {
    return this.http.get<Report[]>(this.reportsUrl)
    .pipe(
      catchError(this.alerte.handleError<Report[]>('getReports', []))
    );
  }

  getReport(id: number|string): Observable<Report> {
    const url = `${this.reportsUrl}/${id}`;
    return this.http
      .get<Report>(url)
      .pipe(
        // tap(_ => this.log(`fetched report id=${id}`)),
        catchError(this.alerte.handleError<Report>(`getReport id=${id}`))
      );
  }


  addReport(report: Report): Observable<Report> {
    return this.http
      .post<Report>(this.reportsUrl, report, this.httpOptions)
      .pipe(
        catchError(this.alerte.handleError<Report>('addReport'))
      );
  }

  /**
   * Récupère un Signalement en fonction de l'email de son auteur
   * 
   * @param email l'email de l'auteur recherché 
   * @returns une liste de Signalement correspondant à la description
   */
  getReportByEmail(email: string): Observable<Report[]> {
    return this.getReports()
      .pipe(
        map(list => list.filter(report => report.author.email === email))
      )
  }

  updateReport(report: Report): Observable<Report> {
    return this.http
      .put<Report>(this.reportsUrl, report, this.httpOptions)
      .pipe(
        catchError(this.alerte.handleError<Report>('updateReport'))
      );
  }

}
