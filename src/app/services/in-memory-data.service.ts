import { Injectable } from '@angular/core';
import { InMemoryDbService } from 'angular-in-memory-web-api';
import { Observable } from 'rxjs';
import { Observation } from '../entities/observation.interface';
import { Report } from '../entities/report.interface';

@Injectable({
  providedIn: 'root'
})
export class InMemoryDataService implements InMemoryDbService {

  constructor() { }

  /**
   * Initialisation de la BD local
   * 
   * @returns les données 
   */
  createDb(): {} | Observable<{}> | Promise<{}> {
    const reports: Report[] = [
      {
        "id" : 1,
        "author": {
          "first_name": "Dominic",
          "last_name": "Carney",
          "email": "nunc.mauris@icloud.edu",
          "birth_day" : new Date("1994-11-01"),
          "sexe" : "Homme",
        },
        "description": "Premier signalement",
        "observations": [
          { id: 1, value: 'Alerte' },
          { id: 3, value: 'Casse' },
          { id: 7, value: 'Urgent' }
        ]
      },
      {
        "id" : 2,
        "author": {
          "first_name": "Eve",
          "last_name": "Haley",
          "email": "rutrum.urna@icloud.ca",
          "birth_day" : new Date("1994-11-01"),
          "sexe" : "Femme",
        },
        "description": "Deuxieme signalement",
        "observations": [
          { id: 2, value: 'Vu' },
          { id: 3, value: 'Casse' }
        ]
      }
    ];

    const observations: Observation[] = [
      { id: 1, value: 'Alerte' },
      { id: 2, value: 'Vu' },
      { id: 3, value: 'Casse' },
      { id: 4, value: 'Remplacement' },
      { id: 5, value: 'Non-prioritaire' },
      { id: 6, value: 'Moyen' },
      { id: 7, value: 'Urgent' }
    ]
   
    return {reports, observations}
  }
}
