import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Observable, of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AlerteSnackBarService {

  constructor(
    private _snackBar: MatSnackBar
  ) { }


  /**
   * Fonction pour notifier l'utilisateur d'une action
   * 
   * @param message le message à transmettre 
   */
  alertMessage(message: string) {
    this._snackBar.open(message, "OK", {
      duration: 5000
    });
  }

  /**
   * Log des erreurs 
   * 
   * @param operation le nom de l'opération qui à planté
   * @param result 
   * @returns 
   */
   handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error); 
      this.alertMessage(`${operation} failed: ${error.message}`);
      return of(result as T);
    }
  }

}
