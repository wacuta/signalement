import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { Injectable } from '@angular/core';
import { Observation } from '../entities/observation.interface';

@Injectable({
  providedIn: 'root'
})
export class ObservationService {

  private observationsUrl : string = 'api/observations';

  constructor(
    private http: HttpClient
  ) { }

  /**
   * Récupère la liste d'Observation en base
   * 
   * @returns un Observable de cette liste
   */
  getObservations(): Observable<Observation[]> {
    return this.http.get<Observation[]>(this.observationsUrl)
  }

}
