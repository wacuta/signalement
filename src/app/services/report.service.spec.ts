import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { ReportService } from './report.service';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { Report } from '../entities/report.interface';

describe('ReportService', () => {
  let service: ReportService;
  let httpMock: HttpTestingController;

  let resultMock: Report[] = [
    {
      "id" : 1,
      "author": {
        "first_name": "Prenom 1",
        "last_name": "Nom 1",
        "email": "e@mail.com",
        "birth_day" : new Date(),
        "sexe" : "Homme",
      },
      "description": "description 1",
      "observations": [
        { id: 1, value: 'Alerte' }
      ]
    },
    {
      "id" : 2,
      "author": {
        "first_name": "Prenom 2",
        "last_name": "Nom 2",
        "email": "e@mail.com",
        "birth_day" : new Date(),
        "sexe" : "Homme",
      },
      "description": "description 2",
      "observations": []
    },
  ];

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        MatSnackBarModule
      ], 
      providers: [ReportService]
    });
    service = TestBed.inject(ReportService);
    httpMock = TestBed.inject(HttpTestingController);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('#getReports doit returner une liste de Report', () => {

    service.getReports().subscribe((reports)=> {
      expect(reports.length).toBe(2)
      expect(reports).toEqual(resultMock)
    });

    const request = httpMock.expectOne( `api/reports`);
    expect(request.request.method).toBe('GET');
    request.flush(resultMock);
  });

  it('#getReport doit returner le report #1', () => {
    let idReport: number = 1
    service.getReport(idReport).subscribe((report)=> {
      expect(report).toEqual(resultMock[1])
    });

    const request = httpMock.expectOne( `api/reports/${idReport}`);
    expect(request.request.method).toBe('GET');
    request.flush(resultMock[1]);
  });

  it('#getReport ne trouve pas le report', () => {
    let idReport: number = 3

    service.getReport(idReport).subscribe({
      next: () => fail("coucou"), 
      error: (err) => expect(err).toBeTruthy()
    });

    const request = httpMock.expectOne( `api/reports/${idReport}` );
    
    expect(request.cancelled).toBeFalsy();
    expect(request.request.responseType).toEqual('json');

  });

  afterEach(() => {
    httpMock.verify();
  })

});
