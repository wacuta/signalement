import { Injectable } from '@angular/core';
import { AbstractControl, FormGroup, ValidationErrors, ValidatorFn } from '@angular/forms';
import { ReportService } from './report.service';

@Injectable({
  providedIn: 'root'
})
export class ValidatorPersoService {

  constructor(
    private reportService: ReportService
  ) { }

  /**
   * Controle si l'element est compris dans une liste
   * 
   * @param arrayTest 
   * @returns 
   */
  inList(arrayTest: any[]): ValidatorFn {
    return (control: AbstractControl) : ValidationErrors | null => {

      const value = control.value;

      if (!value || value == '') return null;

      let inListValide = true
      if(Array.isArray(value)) {
        value.forEach(element => {
          if(arrayTest.indexOf(element) == -1) {
            inListValide = false
          }
        });
      } else {
        inListValide = arrayTest.indexOf(value) != -1;
      }
      
      return !inListValide ? { inList: true }: null;
    }  
  }

  /**
   * Control s'il existe pas déjà un email dans les signaux
   * 
   * @returns 
   */
  controlEmailExist(group: FormGroup): ValidationErrors | null {
    const email = group.controls['email'];

    let emailExist = false;
    this.reportService.getReportByEmail(email.value)
      .subscribe(sign => {
        emailExist = sign.length > 0
        if(emailExist)
              email.setErrors({existe: true})
      })
    
    return null;
  }


  /**
   * Control si la personne est pas un peut trop vielle 
   * max 100 ans
   * 
   * @returns 
   */
  ageValidator(): ValidatorFn {
    return (control: AbstractControl) : ValidationErrors | null => {
      const value = control.value;
      if (!value || value == '') return null;
      let timeDiff = Math.abs(Date.now() - value);
      let age = Math.floor((timeDiff / (1000 * 3600 * 24))/365.25);
      return age > 100 ? { tooOld: true }: null;
    }
  }
}
