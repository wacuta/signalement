import { TestBed } from '@angular/core/testing';
import { MatSnackBarModule } from '@angular/material/snack-bar';

import { AlerteSnackBarService } from './alerte-snack-bar.service';

describe('AlerteSnackBarService', () => {
  let service: AlerteSnackBarService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        MatSnackBarModule
      ]
    });
    service = TestBed.inject(AlerteSnackBarService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
