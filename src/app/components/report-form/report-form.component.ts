import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Gender } from 'src/app/entities/gender.enum';
import { Observation } from 'src/app/entities/observation.interface';
import { Report } from 'src/app/entities/report.interface';
import { AlerteSnackBarService } from 'src/app/services/alerte-snack-bar.service';
import { ObservationService } from 'src/app/services/observation.service';
import { ReportService } from 'src/app/services/report.service';
import { ValidatorPersoService } from 'src/app/services/validator-perso.service';

@Component({
  templateUrl: './report-form.component.html',
  styleUrls: ['./report-form.component.scss']
})
export class ReportFormComponent {

  gender: string[] = Object.values(Gender)
  observationsList: Observation[] = [];
  
  reportForm: FormGroup = this.buildFormReport();

  constructor(
    protected route: ActivatedRoute,
    protected router: Router,
    protected reportService: ReportService,
    private observationService: ObservationService,
    protected reportValidator: ValidatorPersoService,
    private formBuilder: FormBuilder,
    protected alerte: AlerteSnackBarService
  ) {}

  
  
  ngOnInit() : void {
    // charger les observations
    this.loadObservations();
  }


  /**
   * Supprimer une observation de la liste
   * 
   * @param observation l'observation a supprimer de la liste
   */
  removeObservation(observation: string): void {
    const index = this.observationsValid?.value.indexOf(observation);
    if (index >= 0) {
      this.observationsValid?.value.splice(index, 1);
    }
  }


  async saveReport(): Promise<void> {

    await this.reportValidator.controlEmailExist(this.reportForm)
    
    if(!this.reportForm.valid) return;
    
    const report: Report = this.getReportFromForm()

    this.reportService.addReport(report)
      .subscribe(report => {
        this.alerte.alertMessage("Ajout du report #" + report.id)
        this.router.navigate(['/'], { relativeTo: this.route });
      });

  }

  /**
   * Transforme les valeurs du formulaire en un objet Report
   * 
   * @returns le report saisie dans le formulaire
   */
  getReportFromForm(): Report {
    return {
      id: Date.now(),
      author: {
        first_name: this.reportForm.value.first_name,
        last_name: this.reportForm.value.last_name,
        birth_day: this.reportForm.value.birth_day,
        sexe: this.reportForm.value.sexe,
        email: this.reportForm.value.email
      },
      description: this.reportForm.value.description,
      observations: this.reportForm.value.observations
    }
  }

  /**
   * Chargement de la liste des observations
   */
  private loadObservations(): void {
    const observation$ = this.observationService.getObservations(); 
    observation$.subscribe(
      observations => this.observationsList = observations
    );
  }

  /**
   * Construction du formulaire de report
   * 
   * @returns le FormGroup du formulaire
   */
  private buildFormReport(): FormGroup {
    return this.formBuilder.group({
      first_name: ['', {
        validators: [
          Validators.required,
          Validators.maxLength(50)
        ]
      }],
      last_name: ['', {
        validators: [
          Validators.required,
          Validators.maxLength(50)
        ]
      }],
      sexe: ['', {
        validators: [
          Validators.required,
          this.reportValidator.inList(Object.values(Gender))
        ]
      }],
      email: ['', {
        validators: [
          Validators.required,
          Validators.email
        ]
      }],
      birth_day: ['', {
        validators: [
          Validators.required,
          this.reportValidator.ageValidator()
        ]
      }],
      description: ['', { 
        validators: [
          Validators.required
        ]
      }],
      observations: ['']
    });
  }

  /**
   * Getter validators
   */
  get observationsValid() { return this.reportForm.get('observations'); }

}
