import { Component } from '@angular/core';
import { Report } from 'src/app/entities/report.interface';
import { ReportFormComponent } from './report-form.component';

@Component({
    templateUrl: './report-form.component.html',
    styleUrls: ['./report-form.component.scss']
})
export class ReportFormEditComponent extends ReportFormComponent {

    private idReport : number = 0;


    override ngOnInit(): void {
        
        this.route.data.subscribe(({ report }) => {
            this.idReport = report.id
            this.reportForm.setValue({
                first_name: report.author.first_name,
                last_name: report.author.last_name,
                sexe: report.author.sexe,
                email: report.author.email,
                birth_day: report.author.birth_day,
                description: report.description,
                observations: report.observations,
            })
            
        })
    }

    override async saveReport(): Promise<void> {

        await this.reportValidator.controlEmailExist(this.reportForm)
    
        if(!this.reportForm.valid) return;

        const report: Report = this.getReportFromForm()

        this.reportService.updateReport(report)
            .subscribe(report => {
                this.alerte.alertMessage("Modification du report")
                this.router.navigate(['/'], { relativeTo: this.route });
            });

    }

     /**
    * Transforme les valeurs du formulaire en un objet Report
    * 
    * @returns le report saisie dans le formulaire
    */
    override getReportFromForm(): Report {
        return {
            id: this.idReport,
            author: {
                first_name: this.reportForm.value.first_name,
                last_name: this.reportForm.value.last_name,
                birth_day: this.reportForm.value.birth_day,
                sexe: this.reportForm.value.sexe,
                email: this.reportForm.value.email
            },
            description: this.reportForm.value.description,
            observations: this.reportForm.value.observations
        }
   }

}