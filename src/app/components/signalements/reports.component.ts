import { Component } from '@angular/core';
import { ReportService } from 'src/app/services/report.service';
import { Report } from '../../entities/report.interface';

@Component({
  selector: 'reports-list',
  templateUrl: './reports.component.html',
  styleUrls: ['./reports.component.scss']
})
export class ReportsComponent {
  
  reports: Report[] = [];

  constructor(
    private reportService: ReportService
  ) {}

  ngOnInit(): void {
    this.getReports();
  }

  /**
   * Récupérer les reports via le service
   */
  getReports() {
    this.reportService.getReports()
      .subscribe(reports => this.reports = reports);
  }

}
