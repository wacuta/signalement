import { Injectable } from '@angular/core';
import {
  Resolve,
  RouterStateSnapshot,
  ActivatedRouteSnapshot
} from '@angular/router';
import { Observable } from 'rxjs';
import { Report } from '../entities/report.interface';
import { ReportService } from '../services/report.service';

@Injectable({
  providedIn: 'root'
})
export class ReportResolver implements Resolve<Report> {

  constructor(
    private service: ReportService
  ) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Report> {
    const id = route.paramMap.get('id') || '';
    return this.service.getReport(id);
  }


}
