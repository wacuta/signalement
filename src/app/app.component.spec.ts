import { TestBed, ComponentFixture } from '@angular/core/testing';
import { MatIconModule } from '@angular/material/icon';
import { MatToolbarModule } from '@angular/material/toolbar';
import { By } from '@angular/platform-browser';
import { RouterOutlet } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { AppComponent } from './app.component';

let component: AppComponent;
let fixture: ComponentFixture<AppComponent>;

describe('AppComponent', () => {
  
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        MatToolbarModule,
        MatIconModule,
        RouterOutlet,
        RouterTestingModule
      ],
      declarations: [
        AppComponent
      ],
    }).compileComponents();
    fixture = TestBed.createComponent(AppComponent);
    component = fixture.componentInstance;
  });

  it('Création du composent', () => {
    expect(component).toBeTruthy();
  });

  it(`Le titre de l'app est 'Signalement'`, () => {
    expect(component.title).toEqual('Signalement');
  });

  it(`Le titre est dans la toolbar`, ()=> {
    const toolbarTitle = fixture.debugElement.nativeElement.querySelector('mat-toolbar *:first-child')
    fixture.detectChanges(); 
    expect(toolbarTitle.textContent).toEqual(component.title);
  })

  it(`La toolbar à la fonction d'ajout`, ()=> {
    let href = fixture.debugElement
      .query(By.css('mat-toolbar a')).nativeElement
      .getAttribute('routerLink');
    expect(href).toEqual('/add'); 
  })
});
