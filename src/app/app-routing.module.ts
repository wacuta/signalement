import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ReportFormEditComponent } from './components/report-form/report-form-edit.component';
import { ReportFormComponent } from './components/report-form/report-form.component';
import { ReportsComponent } from './components/reports/reports.component';
import { ReportResolver } from './resolvers/report.resolver';

const routes: Routes = [
  { path: '', component: ReportsComponent },
  { path: 'add', component: ReportFormComponent },
  { 
    path: 'edit/:id', 
    component: ReportFormEditComponent,
    resolve : {
      report: ReportResolver
    }
  },
  // { path: '**', component: PageNotFoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
