
export enum Gender {
    Man = "Homme",
    Woman = "Femme",
    Non_Binary = "Non-binaire"
}