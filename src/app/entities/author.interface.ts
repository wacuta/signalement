
export interface Author {

    first_name: string;
    last_name: string;
    birth_day: Date;
    sexe: string;
    email: string;
}