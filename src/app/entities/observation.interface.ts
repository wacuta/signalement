
export interface Observation {

    id: number;
    value: string;
}